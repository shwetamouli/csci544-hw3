# README #

### Updated Approach ###

I used Microsoft's NGram API to check for variants of sentences using the 2 possibilities and updated whenever the alternative had a higher probability.

I chunked the file into smaller bits and concatenated them because the API would sometimes fail and this caused all progress to be lost.

### Approach for HW3 ###

I used NLTK's Brown corpus and Gutenberg Corpus as training data for this assignment. The main features I used were trigrams when the word was not in the beginning or end of sentence and bigrams otherwise. I found the counts in Gutenberg and Brown and chose the variant (to or too, lose or loose etc.) as required at every step. Then I chose between the two based on which one scored higher.

I tried an approach where I fell back on Google's Ngram viewer(as the data set was too huge, I could not process it, I used a python package someone had developed to get data) when both the corpora returned zero and a decision could not be made(as doing it for all data caused too many requests to google)

The main resources are NLTK, and https://github.com/econpy/google-ngrams (though the latter didn't work as expected)
