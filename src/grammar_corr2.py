__author__ = 'shwetamouli'
from getngrams import runQuery
import sys
import io
import re
import time
import nltk
from collections import Counter
from nltk.corpus import brown
from nltk.corpus import gutenberg
from nltk.util import ngrams

#getting all corpora
b_bigrams = ngrams(brown.words(), 2)
b_bigrams_freq = Counter(b_bigrams)

g_bigrams = ngrams(gutenberg.words(), 2)
g_bigrams_freq = Counter(g_bigrams)

g_trigrams = ngrams(gutenberg.words(), 3)
g_trigrams_freq = Counter(g_trigrams)

b_trigrams = ngrams(brown.words(), 3)
b_trigrams_freq = Counter(b_trigrams)

def word_replacement(words, i, x1, x2, y1, y2):
    fallback = False
    string1_sum = 0
    string2_sum = 0
    if i != 0 and i != len(words)-1:
        fallback = True
        if len(words[i-1]) > 0:
            prev_word = words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1]
        else:
            prev_word = words[i-1]
        if len(words[i+1]) > 0:
            next_word = words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1]
        else:
            next_word = words[i+1]
        string1_sum = g_trigrams_freq[(prev_word, x1, next_word)] + b_trigrams_freq[(prev_word, x1, next_word)] + g_trigrams_freq[(prev_word, x2, next_word)] + b_trigrams_freq[(prev_word, x2, next_word)]
        #string1_sum += g_bigrams_freq[(y1, next_word)] + b_bigrams_freq[(y1, next_word)]
        #string1_sum += g_bigrams_freq[(prev_word, y1)] + b_bigrams_freq[(prev_word, y1)]
        string2_sum = g_trigrams_freq[(prev_word, y2, next_word)] + b_trigrams_freq[(prev_word, y2, next_word)] + g_trigrams_freq[(prev_word, y1, next_word)] + b_trigrams_freq[(prev_word, y1, next_word)]
        #string2_sum += g_bigrams_freq[(y2, next_word)] + b_bigrams_freq[(y2, next_word)]
        #string2_sum += g_bigrams_freq[(prev_word, y2)] + b_bigrams_freq[(prev_word, y2)]
    elif i == 0 and i != len(words)-1:
        string1_sum = g_bigrams_freq[(x1, words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1])] + b_bigrams_freq[(x1, words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1])] + g_bigrams_freq[(x2, words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1])] + b_bigrams_freq[(x2, words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1])]
        string2_sum = g_bigrams_freq[(y1, words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1])] + b_bigrams_freq[(y1, words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1])] + g_bigrams_freq[(y2, words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1])] + b_bigrams_freq[(y2, words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1])]
    elif i != 0 and i == len(words) - 1:
        string1_sum = g_bigrams_freq[(words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1], x1)] + b_bigrams_freq[(words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1], x1)] + g_bigrams_freq[(words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1], x2)] + b_bigrams_freq[(words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1], x2)]
        string2_sum = g_bigrams_freq[(words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1], y1)] + b_bigrams_freq[(words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1], y1)] + g_bigrams_freq[(words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1], y2)] + b_bigrams_freq[(words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1], y2)]

    #google api fallback
    if (string1_sum == 0 and string2_sum == 0) and fallback == True:
        #print("Why am i here")
        # if words[i] == x1:
        #     string1 = ((words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1]) if i > 0 else '') + " " + words[i] + " " + ((words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1]) if i < len(words) else None)
        #     string2 = ((words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1]) if i > 0 else '') + " " + y1 + " " + ((words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1]) if i < len(words) else None)
        #     time.sleep(1)
        #     query1 = runQuery(string1)
        #     time.sleep(1)
        #     query2 = runQuery(string2)
        #     sum1 = query1.sum()
        #     sum2 = query2.sum()
        #     try:
        #         if sum2[1] > sum1[1]:
        #             words[i] = y1
        #     except IndexError:
        #         if len(sum1) == 1:
        #             words[i] = y1
        # if words[i] == x2:
        #     string1 = ((words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1]) if i > 0 else '') + " " + words[i] + " " + ((words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1]) if i < len(words) else None)
        #     string2 = ((words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1]) if i > 0 else '') + " " + y2 + " " + ((words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1]) if i < len(words) else None)
        #     #print(string1, string2)
        #     query1 = runQuery(string1)
        #     query2 = runQuery(string2)
        #     sum1 = query1.sum()
        #     sum2 = query2.sum()
        #     try:
        #         if sum2[1] > sum1[1]:
        #             words[i] = y2
        #     except IndexError:
        #         if len(sum1) == 1:
        #             words[i] = y2
        pass
    elif string2_sum > string1_sum:
        if words[i] == x1:
            words[i] = y1
        else:
            words[i] = y2
    else:
        pass
        #words[i] = x1

    return words

inputfile =sys.argv[1]

with io.open(inputfile, 'r', encoding="iso-8859-15") as inpf:
    for line in inpf:
        re.sub('you ?\' ?re', 'you\'re', line)
        re.sub('You ?\' ?re', 'You\'re', line)
        re.sub('they ?\' ?re', 'they\'re', line)
        re.sub('They ?\' ?re', 'They\'re', line)
        re.sub('it ?\' ?s', 'it\'s', line)
        re.sub('It ?\' ?s', 'It\'s', line)
        line = line.rstrip('\n')
        words = line.split(' ')

        i = 0
        while i < len(words):
            if words[i] == 'your' or words[i] == 'Your':
                words = word_replacement(words, i, 'your', 'Your', 'you\'re', 'You\'re')
            elif words[i] == 'you\'re' or words[i] == 'You\'re':
                words = word_replacement(words, i, 'you\'re', 'You\'re', 'your', 'Your')
            if words[i] == 'their' or words[i] == 'Their':
                words = word_replacement(words, i, 'their', 'Their', 'they\'re', 'They\'re')
            elif words[i] == 'they\'re' or words[i] == 'They\'re':
                words = word_replacement(words, i, 'they\'re', 'They\'re', 'their', 'Their')
            if words[i] == 'lose' or words[i] == 'Lose':
                words = word_replacement(words, i, 'lose', 'Lose', 'loose', 'Loose')
            elif words[i] == 'loose' or words[i] == 'Loose':
                words = word_replacement(words, i, 'loose', 'Loose', 'lose', 'Lose')
            if words[i] == 'its' or words[i] == 'Its':
                words = word_replacement(words, i, 'its', 'Its', 'it\'s', 'It\'s')
            elif words[i] == 'it\'s' or words[i] == 'It\'s':
                words = word_replacement(words, i, 'it\'s', 'It\'s', 'its', 'Its')
            if words[i] == 'too' or words[i] == 'Too':
                words = word_replacement(words, i, 'too', 'Too', 'to', 'To')
            elif words[i] == 'to' or words[i] == 'To':
                words = word_replacement(words, i, 'to', 'To', 'too', 'Too')

            i += 1
        print(" ".join(words))
