__author__ = 'shmouli'
import MicrosoftNgram
import os
import sys
import urllib
import urllib2

f = open('errfile', 'w')
x = 0
def word_replacement(words, i, x1, x2, y1, y2, x):
    flag = 0
    if i != 0 and i != len(words)-1:
        flag = 0
        if len(words[i-1]) > 0:
            prev_word = words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1]
        else:
            prev_word = words[i-1]
        if len(words[i-2]) > 0:
            prev1_word = words[i-2] if words[i-2][-1].isalpha() is True else words[i-2][:-1]
        else:
            prev1_word = None
        if len(words[i+1]) > 0:
            next_word = words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1]
        else:
            next_word = words[i+1]
        # if words[i+2] and len(words[i+2]) > 0:
        #     next1_word = words[i+2] if words[i+2][-1].isalpha() is True else words[i+2][:-1]
        # else:
        #     next1_word = None
        try:
            next1_word = words[i+2] if words[i+2][-1].isalpha() is True else words[i+2][:-1]
        except IndexError:
            next1_word = ''

        str1 = (prev1_word+" " if prev1_word is not None else '')+prev_word +" "+ words[i] +" "+ next_word+" "+(next1_word if next1_word is not None else '')
        str2 = (prev1_word+" " if prev1_word is not None else '')+prev_word + " " + y1 + " "+next_word+" "+(next1_word if next1_word is not None else '')
    elif i == 0 and i != len(words)-1:
        flag = 1
        str1 = words[i] + " " + (words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1]) +" "+ (words[i+2] if words[i+2] and words[i+2][-1].isalpha() is True else (words[i+2][:-1] if words[i+2] is True else ''))
        str2 = y2 + " " + (words[i+1] if words[i+1][-1].isalpha() is True else words[i+1][:-1]) +" "+ (words[i+2] if words[i+2] and words[i+2][-1].isalpha() is True else (words[i+2][:-1] if words[i+2] is True else ''))
    elif i != 0 and i == len(words) - 1:
        flag = 1
        str1 = (words[i-2] if words[i-2] and words[i-2][-1].isalpha() is True else (words[i-2][:-1] if words[i-2] is True else ''))+" "+(words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1])+" "+words[i]
        str2 = (words[i-2] if words[i-2] and words[i-2][-1].isalpha() is True else (words[i-2][:-1] if words[i-2] is True else ''))+" "+(words[i-1] if words[i-1][-1].isalpha() is True else words[i-1][:-1])+" "+y1
    strtosearch = str1 + '\n' + str2
    try:
        if flag == 0:
            #print strtosearch
            val = urllib2.urlopen(urllib2.Request('http://weblm.research.microsoft.com/rest.svc/bing-body/2013-12/5/jp?u=dba69388-7723-4afd-95fa-df78ce704603',strtosearch)).read()

        elif flag == 1:
            #print strtosearch
            val = urllib2.urlopen(urllib2.Request('http://weblm.research.microsoft.com/rest.svc/bing-body/2013-12/3/jp?u=dba69388-7723-4afd-95fa-df78ce704603',strtosearch)).read()
        val = val.split('\n')
        val[0] = float(val[0].rstrip('\r'))
        val[1] = float(val[1].rstrip('\r'))
        ind = val.index(max(val))
        if ind == 1:
            #print val
            if words[i] == x1:
                #print words
                words[i] = y1
                #print words
            else:
                #print words
                words[i] = y2
                #print words
    except IOError:
        x = x+1
        f.write(str(x)+" "+str(words)+'\n')
    return words

def checkStr(str1, str2):
    strtosearch = str1 + '\n' + str2
    val = urllib2.urlopen(urllib2.Request('http://weblm.research.microsoft.com/rest.svc/bing-body/2013-12/3/jp?u=dba69388-7723-4afd-95fa-df78ce704603',strtosearch)).read()
    val = val.split('\n')
    val[0] = float(val[0].rstrip('\r'))
    val[1] = float(val[1].rstrip('\r'))
    print val.index(max(val))

#checkStr('this is to much', 'this is too much')

#inputFile = 'hw3.dev.txt'
filelist = os.listdir('alltxt')
for fn in filelist:
    with open('alltxt/'+fn, 'r') as inputFile:
        with open('alltxt/'+fn+'_0', 'w') as outputfile:
            for line in inputFile:
                line = str.replace(line, 'you \' re', 'you\'re')
                line = str.replace(line, 'you\' re', 'you\'re')
                line = str.replace(line, 'you \'re', 'you\'re')
                line = str.replace(line, 'You \' re', 'You\'re')
                line = str.replace(line, 'You\' re', 'You\'re')
                line = str.replace(line, 'You \'re', 'You\'re')
                line = str.replace(line, 'they \' re', 'they\'re')
                line = str.replace(line, 'they\' re', 'they\'re')
                line = str.replace(line, 'they \'re', 'they\'re')
                line = str.replace(line, 'They\' re', 'They\'re')
                line = str.replace(line, 'They \' re', 'They\'re')
                line = str.replace(line, 'They \'re', 'They\'re')
                line = line.rstrip('\n')
                words = line.split(' ')
                i = 0
                while i < len(words):
                    if words[i] == 'your' or words[i] == 'Your':
                        words = word_replacement(words, i, 'your', 'Your', 'you\'re', 'You\'re', x)
                    elif words[i] == 'you\'re' or words[i] == 'You\'re':
                        words = word_replacement(words, i, 'you\'re', 'You\'re', 'your', 'Your', x)
                    if words[i] == 'their' or words[i] == 'Their':
                        words = word_replacement(words, i, 'their', 'Their', 'they\'re', 'They\'re', x)
                    elif words[i] == 'they\'re' or words[i] == 'They\'re':
                        words = word_replacement(words, i, 'they\'re', 'They\'re', 'their', 'Their', x)
                    if words[i] == 'lose' or words[i] == 'Lose':
                        words = word_replacement(words, i, 'lose', 'Lose', 'loose', 'Loose', x)
                    elif words[i] == 'loose' or words[i] == 'Loose':
                        words = word_replacement(words, i, 'loose', 'Loose', 'lose', 'Lose', x)
                    if words[i] == 'its' or words[i] == 'Its':
                        words = word_replacement(words, i, 'its', 'Its', 'it\'s', 'It\'s', x)
                    elif words[i] == 'it\'s' or words[i] == 'It\'s':
                        words = word_replacement(words, i, 'it\'s', 'It\'s', 'its', 'Its', x)
                    if words[i] == 'too' or words[i] == 'Too':
                        words = word_replacement(words, i, 'too', 'Too', 'to', 'To', x)
                    elif words[i] == 'to' or words[i] == 'To':
                        words = word_replacement(words, i, 'to', 'To', 'too', 'Too', x)

                    i += 1
                outputfile.write(" ".join(words)+'\n')
